//    facecoin.js - Face recognition in hash bitmaps as aesthetic proof of work.
//    Copyright (C) 2014, 2020 Rob Myers <rob@robmyers.org>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Configuration
////////////////////////////////////////////////////////////////////////////////

const digest_size = 64;
const bitmap_size = 16;
const canvas_size = 256;
const canvas_scale = canvas_size / bitmap_size;
const blur_radius = 5;
const match_line_width = 2;
const half_match_line_width = match_line_width / 2;
const extra_text_height = 234;
const truncate_blocks_at = 128;

////////////////////////////////////////////////////////////////////////////////
// Globals
////////////////////////////////////////////////////////////////////////////////

// Should encapsulate

let ui;
let matches;
let tries;
let digest;
// Starting hash for Facecoin Cash.
let previousDigest = "5374617274696e67206861736820666f722046616365636f696e20436173682e";

let blurredFaceCanvas;

////////////////////////////////////////////////////////////////////////////////
// Utility code
////////////////////////////////////////////////////////////////////////////////

// Request the next animation frame on any platform version

window.requestAnimFrame = (() => {
                             return window.requestAnimationFrame  ||
                               window.webkitRequestAnimationFrame ||
                               window.mozRequestAnimationFrame    ||
                               function( callback ){
                                 window.setTimeout(callback, 1000 / 60);
                               };
                           })();

////////////////////////////////////////////////////////////////////////////////
// Create UI for each block
////////////////////////////////////////////////////////////////////////////////

const createSection = where => {
  const figure = document.createElement("span");
  $(figure).addClass("page-grid-cell");
  $(figure).width(canvas_size);
  // This is so the first cell isn't shorter than the others
  $(figure).height(canvas_size + extra_text_height);
  const canvas = document.createElement("canvas");
  canvas.width = canvas_size;
  canvas.height = canvas_size;
  const caption = document.createElement("p");
  $(caption).css("word-wrap", "break-word");
  $(caption).width(canvas_size);
  $(figure).append(canvas);
  $(figure).append(caption);
  $(where).append(figure);
  return {figure: figure, canvas: canvas, caption: caption,
          ctx: canvas.getContext('2d')};
};

////////////////////////////////////////////////////////////////////////////////
// The digest
////////////////////////////////////////////////////////////////////////////////

const newDigest = () => {
  const hash = sjcl.hash.sha256.hash(previousDigest + tries);
  const digest = sjcl.codec.hex.fromBits(hash);
  // Ensure the first and subsequent layouts line up
  const prev = previousDigest ? previousDigest : "None<br /><br />" ;
  ui.caption.innerHTML = "<b>Previous&nbsp;Digest:</b>&nbsp;" + prev +
    "&nbsp;<br /><b>Proof-of-Work Nonce:</b>&nbsp;" + tries.toString(16) +
    "<br /><b>Proof&#8209;of&#8209;Work&nbsp;Hash:</b>&nbsp;" + digest;
  return digest;
};

////////////////////////////////////////////////////////////////////////////////
// Drawing the digest as a bitmap
////////////////////////////////////////////////////////////////////////////////

const pixelValue = (x, y, bitmap_width, digest) => {
  const byte_index = Math.floor((x + (y * bitmap_width)) / 4);
  const bit_index = (x + (y * bitmap_width)) % 4;
  const grey = ((parseInt(digest[byte_index], 16) >> bit_index) & 0x01) * 255;
  return grey;
};

const drawFace = (ui, digest) => {
   for(let y = 0; y < bitmap_size; y++) {
    for (let x = 0; x < bitmap_size; x++) {
      const grey = pixelValue(x, y, bitmap_size, digest);
      // Slower than other alternatives, but clear
      ui.ctx.fillStyle = "rgb(" + grey +"," + grey + "," + grey + ")";
      ui.ctx.fillRect(x * canvas_scale, y * canvas_scale,
                      canvas_scale, canvas_scale);
    }
  }
};

const copyFaceBlurred = (srcCanvas, destCanvas) => {
  const destCtx = destCanvas.getContext('2d');
  destCtx.drawImage(srcCanvas, 0, 0);
  stackBlurCanvasRGB(destCtx, 0, 0, canvas_size, canvas_size, blur_radius);
};

////////////////////////////////////////////////////////////////////////////////
// Detecting the face in the digest bitmap
////////////////////////////////////////////////////////////////////////////////

const detectFace = canvas => {
  copyFaceBlurred(canvas, blurredFaceCanvas);
  const matches = ccv.detect_objects(
    { "canvas" : ccv.grayscale(ccv.pre(blurredFaceCanvas)),
      "cascade" : cascade,
      "interval" : 5,
      "min_neighbors" : 1 });
  return matches;
};

const drawMatches = (ui, matches) => {
  // Just draw the first one
  // In testing, multiples were overlapping matches of the same feature
  const match = matches[0];
  //matches.forEach(function(match) {
  // Un-clamped values, for comparison
  /*console.log(match);
  ui.ctx.lineWidth = match_line_width;
  ui.ctx.strokeStyle = "rgb(0, 0, 255)";
  // Inset the box, especially for bitmap edges so lines are always same width
  ui.ctx.rect(
    match.x,
    match.y,
    match.width,
    match.height
  );*/
  // Clamp to bitmap pixel boundaries
  const x = Math.round(match.x / canvas_scale);
  const y = Math.round(match.y / canvas_scale);
  const width = Math.round(match.width / canvas_scale);
  const height = Math.round(match.height / canvas_scale);
  ui.ctx.lineWidth = match_line_width;
  ui.ctx.strokeStyle = "rgb(255, 0, 0)";
  // Inset the box, especially for bitmap edges so lines are always same width
  ui.ctx.rect(
    (x  * canvas_scale) + half_match_line_width,
    (y * canvas_scale) + half_match_line_width,
    (width * canvas_scale) - half_match_line_width,
    (height * canvas_scale) - half_match_line_width
  );
  ui.ctx.stroke();
  // LTRB "litterbug" order co-ordinates (makes sense for top left 0, 0)
  ui.caption.innerHTML +=
    `<br /><b>Face:</b>&nbsp; ${x}, ${y}, ${x + width}, ${y + height}`;
  //});
};

////////////////////////////////////////////////////////////////////////////////
// Main flow of execution
////////////////////////////////////////////////////////////////////////////////

const nextBlock = () => {
  // Don't add too many elements to the page, we don't want to hog memory
  if ($('#blocks').find('.page-grid-cell').size() >= truncate_blocks_at) {
    $("#blocks").find(".page-grid-cell:lt(" +
                      Math.floor(truncate_blocks_at / 2) + ")").remove();
  }
  // Set up the state for the new block
  matches = Array();
  tries = 0;
  // Create the ui section for the new block
  ui = createSection("#blocks");
  $('html, body').animate({scrollTop: $(document).height()}, 'slow');
  // And do the work
  animationLoop();
};

const animationLoop = () => {
  if (matches.length == 0) {
    requestAnimFrame(animationLoop);
    tries = tries + 1;
    digest = newDigest();
    drawFace(ui, digest);
    matches = detectFace(ui.canvas);
  } else {
    drawMatches(ui, matches);
    previousDigest = digest;
    nextBlock();
  }
};

$(() => {
  blurredFaceCanvas = document.createElement('canvas');
  blurredFaceCanvas.width = canvas_size;
  blurredFaceCanvas.height = canvas_size;
  nextBlock();
});
